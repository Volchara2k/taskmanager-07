# TaskManager-07

# О проекте
**Менеджер задач**

### Стек технологий:
Java SE/Spring/SPRING REST/SPRING CLOUD/SPRING TESTING/SPRING SECURITY/
SPRING SOAP/SPRING AOP/SPRING IOC/Docker/Git/MySql/PostgreSql/Maven 3/GRADLE/
HIBERNATE

### Программные требования:
- JDK 1.8;
- MS Windows 10 x64.

### Аппаратные требования:
- Процессор: Intel® Dual-Core 2.4 GHz - или аналог;
- Оперативная память: 1.5 GB; 
- Место на диске: менее 5 МБ.

### Сведения о разработчике:
**ФИО**: Волков Валерий Сергеевич

**Электронная почта**: volkov.valery2013@yandex.ru

### Команда для сборки приложения:
```bash
mvn clean install
```

### Команда для запуска приложения:
```bash
java -jar ./taskmanager.jar
```
Приложение поддерживает следующие команды/программные аргументы для получения данных о приложении:
- help - для получения справки;
- version - для получения версии приложения;
- about - для получения информации о разработчике;
- exit - для выхода из приложения.

### Текущая сборка CI / CD:
https://gitlab.com/Volchara2k/taskmanager-07/-/pipelines

## Скриншоты:
https://drive.google.com/drive/folders/1UnaeS4Q4vIfRb0SGhlAybdAsovr7rC4f?usp=sharing