package ru.renessans.jvschool.volkov.task.manager.printer;

import ru.renessans.jvschool.volkov.task.manager.constant.DeveloperConst;
import ru.renessans.jvschool.volkov.task.manager.constant.NotificationConst;
import ru.renessans.jvschool.volkov.task.manager.constant.CommandConst;

import java.util.Scanner;

final public class AppDataPrinter implements CommandConst, NotificationConst, DeveloperConst {

    public void print(final String... args) {
        if (isEmptyArgs(args)) {
            commandPrintLoop();
        } else {
            final String arg = args[0];
            System.out.println(appDataByFactor(arg));
        }
        System.out.println(appDataByFactor(null));
    }

    private boolean isEmptyArgs(final String... args) {
        return args == null || args.length < 1;
    }

    private void commandPrintLoop() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!EXIT_FACTOR.equals(command)) {
            System.out.println((String.format(ENTER_FORMAT_MSG_FACTOR, EXIT_FACTOR, HELP_FACTOR)));
            command = scanner.nextLine();
            System.out.println(appDataByFactor(command));
        }
    }

    private String appDataByFactor(final String factor) {
        if (isEmptyFactor(factor))
            return NO_COMMAND_MSG;

        switch (factor) {
            case HELP_FACTOR:
                return String.format(FORMAT_MSG_HELP, VERSION_FACTOR, ABOUT_FACTOR, HELP_FACTOR);
            case VERSION_FACTOR:
                return VERSION_MSG;
            case ABOUT_FACTOR:
                return String.format(FORMAT_MSG_ABOUT, DEVELOPER, DEVELOPER_MAIL);
            case EXIT_FACTOR:
                return EXIT_MSG;
            default:
                return String.format(FORMAT_MSG_UNKNOWN, factor);
        }
    }

    private boolean isEmptyFactor(final String factor) {
        return factor == null || factor.isEmpty();
    }

}