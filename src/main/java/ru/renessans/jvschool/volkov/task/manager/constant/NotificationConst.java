package ru.renessans.jvschool.volkov.task.manager.constant;

public interface NotificationConst {

    String FORMAT_MSG_UNKNOWN = "Неизвестный аргумент: %s";

    String FORMAT_MSG_HELP =
            "%s - для вывода версии программы; \n" +
                    "%s - для информации о разработчике; \n" +
                    "%s - для вывода списка команд.";

    String FORMAT_MSG_ABOUT = "%s - разработчик; \n%s - почта.";

    String ENTER_FORMAT_MSG_FACTOR =
            "\nВведите команду для получения данных о приложении.\n" +
                    "\tВведите команду \"%s\" для выхода из приложения, \"%s\" - для получения справки.\n";

    String VERSION_MSG = "Версия: 1.0.7";

    String EXIT_MSG = "Выход из приложения";

    String NO_COMMAND_MSG = "Входной аргумент отсутствует!";

}