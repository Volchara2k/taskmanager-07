package ru.renessans.jvschool.volkov.task.manager;

import ru.renessans.jvschool.volkov.task.manager.printer.AppDataPrinter;

public class Application {

    public static void main(final String[] args) {
        final AppDataPrinter printer = new AppDataPrinter();
        printer.print(args);
    }

}