package ru.renessans.jvschool.volkov.task.manager.constant;

public interface CommandConst {

    String HELP_FACTOR = "help";

    String VERSION_FACTOR = "version";

    String ABOUT_FACTOR = "about";

    String EXIT_FACTOR = "exit";

}